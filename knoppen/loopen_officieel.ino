int KN    = 3   ; 
int KZ    = 4   ;
int KW    = 5   ;
int KO    = 6   ;
int LN    = 7   ;
int LZ    = 8   ;
int LW    = 9   ;
int BUZZER= 10  ;
int LO    = 11  ;
int tijd1 = 1500;
int TIJD2 = 2000;

int POS_LEEG=0;
int POS_N=1;
int POS_O=2;
int POS_Z=4;
int POS_W=3;

int positie = POS_LEEG;/*kolom*/

int T = 100;/*voor de tijd dat er bijkomt of afgaat*/
boolean starten =  true;/* voor de start*/
int p = 0;/*punten*/

void pos(int L,int K, int pos){
  digitalWrite (L, HIGH);
    Serial.println("pos="+String(pos));
    delay(tijd1);
    if (digitalRead(K) == HIGH) {
      Serial.println("pos yay ");
      tijd1 = tijd1 - T;
      p++;
      if (tijd1 <= 0) {
        tijd1 = T;
      }
      positie = random(POS_N, POS_Z);
     Serial.println("pos done p="+String(p)+",  tijd="+String(tijd1)+",  pos="+String(positie));
    }
    else {
      Serial.println("pos ohh ");
      positie =  POS_LEEG;
    }
    digitalWrite (L, LOW);
    delay (TIJD2) ;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(KN , INPUT);
  pinMode(KZ , INPUT);
  pinMode(KW , INPUT);
  pinMode(KO , INPUT);
  pinMode(LN , OUTPUT);
  pinMode(LN , OUTPUT);
  pinMode(LW , OUTPUT);
  pinMode(LO , OUTPUT);
  pinMode(BUZZER , OUTPUT);
  Serial.begin(9600);
    Serial.println("READY");
}

void start(){
    Serial.println("starten begin");
    delay (TIJD2);
    digitalWrite (LZ, HIGH);
    delay(1500);
    if (digitalRead(KZ) == LOW) {
      Serial.println("starten knop uit");
      digitalWrite (LZ, LOW);
      positie =  POS_LEEG;
    }
    else {
      Serial.print("starten knop in");
      positie = random(POS_N, POS_W);
      tijd1=1000;
      delay (TIJD2);
      digitalWrite (LZ, LOW);
      delay (TIJD2);
      Serial.println("positie="+String(positie));
      Serial.println("starten knop einde");
      starten = false; 
    }
}
//dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
void loop() {
  // put your main code here, to run repeatedly:
  if (starten) {
    start();
    return;
  }
  if (positie==POS_Z){
    pos(LZ,KZ,POS_Z);
  }
  else if (positie==POS_N){
    pos(LN,KN,POS_N);
  } 
  if (positie==POS_W){
    pos(LW,KW,POS_W);
  } 
  if(positie==POS_O){
    pos(LO,KO,POS_O);
  }
  
  if (positie==POS_LEEG){
      Serial.println("positie leeg");
    for (int count = 0; count < p; count++) {
      digitalWrite (LN,HIGH);
      digitalWrite (LZ,HIGH);
      digitalWrite (LW,HIGH);
      digitalWrite (LO,HIGH);
      tone (BUZZER,1000);
      delay (500);
      digitalWrite (LN,LOW);
      digitalWrite (LZ,LOW);
      digitalWrite (LW,LOW);
      digitalWrite (LO,LOW);
      noTone (BUZZER);
      delay(500);
    }
    p=0;
    starten = true;
  }
}
