int BUZZER = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(BUZZER,OUTPUT);
}
void loop() {
  // put your main code here, to run repeatedly:
  tone(BUZZER,293.6	);
  delay(1000);
  tone(BUZZER,391.9);
  delay(500);
  noTone(BUZZER);
  delay(50);
  tone(BUZZER,391.9);
  delay(250);
  noTone(BUZZER);
  delay(50);
  tone(BUZZER,391.9);
  delay(1000);


}