int servoV = 4 ;
int servoA = 5 ;
int licht  = 6 ;
int lichtK = 7 ;
int knop   = 8 ;
int begin  = 45 ;
int einde  = 135;
int opnieuw= 0 ;
void setup() {
  // put your setup code here, to run once:

  pinMode (servoA,OUTPUT);
  pinMode (servoV,OUTPUT);
  pinMode (licht ,OUTPUT);
  pinMode (lichtK,OUTPUT);
  pinMode (knop  ,INPUT );
  servoA.write (90);
  servoV.write (90);
}

void loop() {
  // put your main code here, to run repeatedly:

  if (digitalRead(knop)== HIGH){
    if (opnieuw < 10) {
      servoV.write(einde);
      digitalWrite(lichtK,HIGH);
      servoA.write(einde);
      digitalWrite(licht,HIGH);
      delay (1000);
      digitalWrite(licht,LOW);
      digitalWrite(lichtK,LOW);
      delay(1000)
      servoV.write(begin);
      digitalWrite(lichtK,HIGH);
      servoA.write(begin);
      digitalWrite(licht,HIGH);
      delay (1000);
      digitalWrite(licht,LOW);
      digitalWrite(lichtK,LOW);
      delay(1000)
      opnieuw++;
    }
    opnieuw=0;
    if (opnieuw < 5){
      servoV.write(einde);
      delay(1500);
      servoA.write(einde);
      delay(1500);
      servoV.write(begin);
      delay(1500);
      servoA.write(begin);
      delay(1500);
      opnieuw++;
    }  
    opnieuw=0;
    if (opnieuw < 5){
      digitalWrite(lichtK,HIGH);
      delay(500);
      digitalWrite(licht,HIGH);
      delay(500);
      digitalWrite(lichtK,LOW);
      delay(500);
      digitalWrite(licht,LOW);
      delay(500);
      opnieuw++;
    }
    opnieuw=0;
  }
}
